'use strict';

var gulp = require('gulp');
var watch = require('gulp-watch');
var stylus = require('gulp-stylus');
var livereload = require('gulp-livereload');
var minifyCss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');

var plugins = require("gulp-load-plugins")({
    pattern: ['gulp-*', 'gulp.*'],
    replaceString: /\bgulp[\-.]/
});

var distDirectory = 'public_html/dist/';
var devDirectory = 'public_html/dev/';

var mainStyleSrc = devDirectory + 'style/style.styl';
// var ieStyleSrc = devDirectory + 'style/style-ie.styl';
var jsDevDirectory = devDirectory + 'js/**/*.js';

gulp.task('stylus', function( done ) {

    var plugins = [ autoprefixer ];

    gulp.src(mainStyleSrc)
        .pipe(sourcemaps.init())
        .pipe(stylus())
        .on('error', displayError)
        .pipe(postcss(plugins))
        .pipe(minifyCss())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(distDirectory + 'css/'))
        .pipe(livereload());

    
    // gulp.src(ieStyleSrc)
    //     .pipe(sourcemaps.init())
    //     .pipe(stylus())
    //     .on('error', displayError)
    //     .pipe(postcss(plugins))
    //     .pipe(minifyCss())
    //     .pipe(sourcemaps.write())
    //     .pipe(gulp.dest(distDirectory + 'css/'))
    //     .pipe(livereload());

    done();

});

gulp.task('js', function( done ) {

    gulp.src(jsDevDirectory)
        .pipe(jshint())
        .pipe(jshint.reporter())
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .on('error', displayError)
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(distDirectory + 'js/'))
        .pipe(livereload());
    
    done();

});

gulp.task('watch', function( done ) {

    livereload.listen();
    gulp.watch( devDirectory + 'js/**/*.js', gulp.series('js'));
    gulp.watch( devDirectory + 'style/**/*.styl', gulp.series('stylus'));
    
    done();

});

function displayError(error) {

    console.log(error.toString()) 
    this.emit('end');

}

gulp.task('dev', gulp.series('watch'));